
const FIRST_NAME = "Violeta-Maria";
const LAST_NAME = "Bondari";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
class Employee {
   
    constructor(name,surname,salary){
    this.name = name;
    this.surname = surname;
    this.salary = salary;
   }

   get SetName()
   {
       return this.name;
   }
   set SetName(value)
   {
       this.name = value;
   }
   get Surname()
   {
       return this.surname;
   }
   set SetSurname(value)
   {
       this.surname = value;
   }
   get Salary()
   {
       return this.salary;
   }
   set SetSalary(value)
   {
       this.salary = value;
   }

   getDetails(){
       return this.name + " " + this.surname + " " + this.salary;
   }
}

class SoftwareEngineer extends Employee {
   
    constructor(name,surname,salary,experience){
        super(name,surname,salary);
        this.experience = experience || 'JUNIOR';
    }

    get Experience()
    {
        return this.experience;
    }
    set Experience(value)
    {
        this.experience = value;
    }

    applyBonus(){
        if(this.experience == 'JUNIOR') {
            return this.salary + this.salary*0.1; 
        }
        else if(this.experience == 'MIDDLE')
        {
            return this.salary + this.salary*0.15;
        }
        else if(this.experience == 'SENIOR')
        {
            return this.salary + this.salary*0.2;
        }
        else{
           return this.salary+this.salary*0.1;
        }
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

